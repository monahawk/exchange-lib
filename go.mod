module gitlab.com/monahawk/exchange-lib

go 1.17

require github.com/sirupsen/logrus v1.9.0

require (
	github.com/cockroachdb/apd v1.1.0 // indirect
	github.com/leekchan/accounting v1.0.0 // direct
	github.com/pkg/errors v0.8.1 // indirect
	github.com/shopspring/decimal v1.3.1 // direct
	github.com/stretchr/testify v1.8.2
	golang.org/x/sys v0.0.0-20220715151400-c0bba94af5f8 // indirect
)

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
