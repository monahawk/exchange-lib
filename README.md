# exchange-lib

[![Build Status](https://gitlab.com/monahawk/exchange-lib/badges/master/pipeline.svg)](https://gitlab.com/monahawk/exchange-lib/commits/master) [![Coverage Report](https://gitlab.com/monahawk/exchange-lib/badges/master/coverage.svg)](https://gitlab.com/monahawk/exchange-lib/commits/master) [![Go Report Card](https://goreportcard.com/badge/gitlab.com/monahawk/exchange-lib)](https://goreportcard.com/report/gitlab.com/monahawk/exchange-lib)

Library for Crypto Exchange

## Getting started
