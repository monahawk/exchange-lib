PROJECT_NAME := exchange-lib
PKG := "gitlab.com/monahawk/$(PROJECT_NAME)"
PKG_LIST := $(shell go list $(PKG)/... | grep -v /vendor/)
GO_FILES := $(shell find . -name '*.go' | grep -v /vendor/ | grep -v _test.go)
VERSION=v0.0.2

.PHONY: test lint

all: build

msan:
	go test -msan -short ${PKG_LIST}

race:
	go test -race -short ${PKG_LIST}

cover: test
	go tool cover -func=cover/coverage.cov

cover_html: test
	go tool cover -html=cover/coverage.cov -o cover/coverage.html

test:
	go test -covermode=count -coverprofile=cover/coverage.cov ./...

lint:
	golangci-lint run ./...

dep:
	go get -v -d ./...

build: dep
	go build -i -v $(PKG)
