package constants

import "github.com/shopspring/decimal"

// The ChainID of the crypto network
type ChainID uint

const (
	MAINNET    ChainID = 11111    // MAINNET Tron Mainnet
	NILE       ChainID = 20190292 // NILE Tron Nile testnet
	SHASTA     ChainID = 1        // SHASTA Tron Shasta testnet
	BSCMainnet ChainID = 56       // BSCMainnet Binance Mainnet
	BSCTestnet ChainID = 243      // BSCTestnet Binance Testnet
	BTTMainnet ChainID = 199      // BTTMainnet Bittorrent Mainnet
	BTTTestnet ChainID = 232      // BTTTestnet Bittorrent Testnet
)

// TradeType is the type of the exchange trade
type TradeType int

const (
	ExactInput  TradeType = iota // ExactInput the input trade amount must be exactly this amount
	ExactOutput                  // ExactOutput the output trade amount must be exactly this amount
)

// Rounding is the fraction rounding
type Rounding int

const (
	RoundDown   Rounding = iota // RoundDown rounding from .5 downwards
	RoundHalfUp                 // RoundHalfUp rounding from .5 upwards
	RoundUp                     // RoundUp rounding from .5 upwards
)

// RelevantUSDCoins returns the USD coins used for price conversion
func RelevantUSDCoins(chainID ChainID) []string {
	switch chainID {
	case MAINNET, SHASTA, NILE:
		return []string{"USDT", "USDD"}
	case BSCMainnet, BSCTestnet:
		return []string{"BUSD"}
	case BTTMainnet, BTTTestnet:
		return []string{"USDT_t", "USDT_b"}
	default:
		return []string{"USDT"}
	}
}

const (
	NTWO     int64 = 2    // NTWO the number 2
	NFIVE    int64 = 5    // NFIVE the number 5
	NTEN     int64 = 10   // NTEN the number 10
	NHUNDRED int64 = 100  // NHUNDRED the number 100
	N997     int64 = 997  // N997 the number 997
	N1000    int64 = 1000 // N1000 the number 1000
)

var (
	TWO     decimal.Decimal = decimal.NewFromInt(NTWO)     // TWO the decimal of 2
	TEN     decimal.Decimal = decimal.NewFromInt(NTEN)     // TEN the decimal of 10
	HUNDRED decimal.Decimal = decimal.NewFromInt(NHUNDRED) // HUNDRED the decimal of 100
)
