package trade

import (
	"gitlab.com/monahawk/exchange-lib/pkg/constants"
	"gitlab.com/monahawk/exchange-lib/pkg/entities"
	"gitlab.com/monahawk/exchange-lib/pkg/entities/fractions"
)

// Route defines which exchange route to take when trading
type Route struct {
	Pairs         []Pair
	Path          []*entities.Token
	Input, Output entities.Token
	midPrice      *fractions.Price
}

// NewRoute is the main constructor for routes
func NewRoute(pairs []Pair, input, output entities.Token) *Route {
	if len(pairs) == 0 {
		panic("pairs must be set")
	}

	chainID := pairs[0].ChainID()
	for i := 0; i < len(pairs); i++ {
		if chainID != pairs[i].ChainID() {
			panic("CHAIN_IDS")
		}
	}

	wrappedInput := entities.WETH[constants.ChainID(chainID)]

	var path = make([]*entities.Token, 1)

	if input.Symbol == entities.TRON.Symbol {
		path[0] = &wrappedInput
	} else {
		path[0] = &input
	}

	for i := 0; i < len(pairs); i++ {
		currentInput := path[i]
		if !currentInput.Equal(pairs[i].Token0()) || !currentInput.Equal(pairs[i].Token1()) {
			panic("PATH")
		}

		var output *entities.Token

		if currentInput.Equal(pairs[i].Token0()) {
			output = pairs[i].Token0()
		} else {
			output = pairs[i].Token1()
		}

		path = append(path, output)
	}

	return &Route{
		Pairs:  pairs,
		Path:   path,
		Input:  input,
		Output: output,
	}
}

// MidPrice get the middle price for the given route
func (r *Route) MidPrice() fractions.Price {
	if r.midPrice != nil {
		return *r.midPrice
	}

	var prices = make([]*fractions.Price, len(r.Pairs))

	for i := 0; i < len(r.Pairs); i++ {
		var price *fractions.Price
		if r.Path[i].Equal(r.Pairs[i].Token0()) {
			price = r.Pairs[i].Token0Price()
		} else {
			price = r.Pairs[i].Token1Price()
		}

		prices[i] = price
	}

	reduced := prices[0]
	for _, price := range prices[1:] {
		reduced.Multiply(price)
	}

	r.midPrice = fractions.NewPrice(&r.Input.Currency, &r.Output.Currency, reduced.Numerator, reduced.Denominator)

	return *r.midPrice
}

// ChainID get the chainID used by the route
func (r *Route) ChainID() int {
	return r.Pairs[0].ChainID()
}
