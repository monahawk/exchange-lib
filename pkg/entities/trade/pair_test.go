package trade

import (
	"math/big"
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/monahawk/exchange-lib/pkg/constants"
	"gitlab.com/monahawk/exchange-lib/pkg/entities"
	"gitlab.com/monahawk/exchange-lib/pkg/entities/fractions"
)

var (
	minTokenAmount = fractions.NewTokenAmount(
		entities.NewToken(
			constants.BSCMainnet,
			"0x01",
			6,
			"Test 1",
			"TOne",
		),
		big.NewInt(10_000_000_000),
	)
	token0Amount = fractions.NewTokenAmount(
		entities.NewToken(
			constants.BSCMainnet,
			"0x01",
			6,
			"Test 1",
			"TOne",
		),
		big.NewInt(1_000_000_000_000),
	)
	token1Amount = fractions.NewTokenAmount(
		entities.NewToken(
			constants.BSCMainnet,
			"0x02",
			6,
			"Test 2",
			"TTwo",
		),
		big.NewInt(10000_000_000_000_000),
	)
	testPairs = []*Pair{
		NewPair(token0Amount, token1Amount, "0xPair1"),
		NewPair(token0Amount, token1Amount, "0xPair2"),
	}
)

func TestGetReserveOf(t *testing.T) {
	require := require.New(t)
	reserve := testPairs[0].ReserveOf(&token0Amount.Token)

	isEqual := reserve.Token.Equal(&token0Amount.Token)
	require.True(isEqual)

}

func TestGetOutputAmount(t *testing.T) {
	require := require.New(t)

	pair := testPairs[0]
	var previousAmount *fractions.TokenAmount
	for i := 0; i < 15; i++ {
		outputAmount, newPair := pair.GetOutputAmount(minTokenAmount)
		pair = &newPair

		t.Logf("Reserve0: %s", pair.Reserve0().ToSignificant(0, nil, fractions.RoundUp))
		t.Logf("Reserve1: %s", pair.Reserve1().ToSignificant(0, nil, fractions.RoundUp))
		t.Logf("OutputAmount: %s", outputAmount.ToSignificant(0, nil, fractions.RoundUp))
		require.Greater(outputAmount.Numerator.Cmp(big.NewInt(0)), 0, "incorrect return amount")
		if previousAmount != nil {
			require.True(outputAmount.LessThan(&previousAmount.Fraction), "should be greater")
		}
		previousAmount = outputAmount
	}
}

func TestGetInputAmount(t *testing.T) {
	require := require.New(t)

	pair := testPairs[0]
	var previousAmount *fractions.TokenAmount
	for i := 0; i < 15; i++ {
		inputAmount, newPair := pair.GetInputAmount(minTokenAmount)
		pair = &newPair

		t.Logf("Reserve0: %s", pair.Reserve0().ToSignificant(0, nil, fractions.RoundUp))
		t.Logf("Reserve1: %s", pair.Reserve1().ToSignificant(0, nil, fractions.RoundUp))
		t.Logf("InputAmount: %s", inputAmount.ToSignificant(0, nil, fractions.RoundUp))
		require.Greater(inputAmount.Numerator.Cmp(big.NewInt(0)), 0, "incorrect return amount")
		if previousAmount != nil {
			require.True(inputAmount.GreaterThan(&previousAmount.Fraction), "should be greater")
		}
		previousAmount = inputAmount
	}
}
