package trade

import (
	"math/big"

	"github.com/sirupsen/logrus"
	"gitlab.com/monahawk/exchange-lib/pkg/constants"
	"gitlab.com/monahawk/exchange-lib/pkg/entities"
	"gitlab.com/monahawk/exchange-lib/pkg/entities/fractions"
)

// Pair the token pair used for exchange
type Pair struct {
	LiquidityToken entities.Token        `json:"liquidityToken,omitempty"`
	TokenAmount0   fractions.TokenAmount `json:"tokenAmount0,omitempty"`
	TokenAmount1   fractions.TokenAmount `json:"tokenAmount1,omitempty"`
}

// NewPair the main constructor for Pairs
func NewPair(tokenAmount0, tokenAmount1 *fractions.TokenAmount, pairAddress string) *Pair {
	return &Pair{
		LiquidityToken: entities.NewToken(tokenAmount0.Token.ChainID, pairAddress, entities.ISWAPDecimals, "Iswap LP", "Iswap LP Token"),
		TokenAmount0:   *tokenAmount0,
		TokenAmount1:   *tokenAmount1,
	}
}

// ChainID get the chainID of the pair
func (p *Pair) ChainID() int {
	return int(p.Token0().ChainID)
}

// Token0 the first of the token pair
func (p *Pair) Token0() *entities.Token {
	return &p.TokenAmount0.Token
}

// Token1 the second of the token pair
func (p *Pair) Token1() *entities.Token {
	return &p.TokenAmount1.Token
}

// Token0Price the price of the first token
func (p *Pair) Token0Price() *fractions.Price {
	return fractions.NewPrice(&p.Token0().Currency, &p.Token1().Currency, p.TokenAmount0.Raw(), p.TokenAmount1.Raw())
}

// Token1Price the price of the second token
func (p *Pair) Token1Price() *fractions.Price {
	return fractions.NewPrice(&p.Token1().Currency, &p.Token0().Currency, p.TokenAmount1.Raw(), p.TokenAmount0.Raw())
}

// PriceOf the the current price of the input token
func (p *Pair) PriceOf(token *entities.Token) fractions.Price {
	if !p.InvolvesToken(token) {
		panic("Invalid token")
	}

	if token.Equal(p.Token0()) {
		return *p.Token0Price()
	}

	return *p.Token1Price()
}

// Reserve0 get the reserve amount of the first token
func (p *Pair) Reserve0() *fractions.TokenAmount {
	return &p.TokenAmount0
}

// Reserve1 get the reserve amount of the second token
func (p *Pair) Reserve1() *fractions.TokenAmount {
	return &p.TokenAmount1
}

// ReserveOf get the reserve of the input token
func (p *Pair) ReserveOf(token *entities.Token) *fractions.TokenAmount {
	if !p.InvolvesToken(token) {
		panic("Invalid token")
	}

	if token.Equal(p.Token0()) {
		return &p.TokenAmount0
	}

	return &p.TokenAmount1
}

// InvolvesToken check wether the input token is part of the pair
func (p *Pair) InvolvesToken(token *entities.Token) bool {
	return token.Equal(p.Token0()) || token.Equal(p.Token1())
}

// GetOutputAmount get the output amount token in exchange for the input token
//
// Example: BNB-BTT pair
// GetOutputAmount(2 BNB) should check the current price of the BNB-BTT pair
// And return the exchangable amount of BTT as result
func (p *Pair) GetOutputAmount(inputAmount *fractions.TokenAmount) (*fractions.TokenAmount, Pair) {
	if !p.InvolvesToken(&inputAmount.Token) {
		panic("Invalid token")
	}

	if p.Reserve0().Quotient().BitLen() == 0 ||
		p.Reserve1().Quotient().BitLen() == 0 {
		panic("Insufficient reserves")
	}

	inputReserve := p.ReserveOf(&inputAmount.Token)

	var outputReserve *fractions.TokenAmount

	if inputAmount.Token.Equal(&p.TokenAmount0.Token) {
		outputReserve = p.Reserve1()
	} else {
		outputReserve = p.Reserve0()
	}

	inputAmountWithFee := new(big.Int).Mul(inputAmount.Quotient(), big.NewInt(constants.N997))
	numerator := new(big.Int).Mul(inputAmountWithFee, outputReserve.Quotient())
	denominator := new(big.Int).Add(new(big.Int).Mul(inputReserve.Quotient(), big.NewInt(constants.N1000)), inputAmountWithFee)

	outputAmount := fractions.NewTokenAmount(outputReserve.Token,
		new(big.Int).Div(numerator, denominator),
	)

	if len(outputAmount.Raw().Bits()) == 0 {
		panic("Insufficient Input amount")
	}

	return outputAmount, *NewPair(inputReserve.Add(inputAmount), outputReserve.Subtract(outputAmount), p.LiquidityToken.Address)
}

// GetInputAmount get the input amount token in exchange for the output token
//
// Example: BNB-BTT pair
// GetInputAmount(2 BNB) should check the current price of the BNB-BTT pair
// And return the exchangable amount of BTT as result
func (p *Pair) GetInputAmount(outputAmount *fractions.TokenAmount) (*fractions.TokenAmount, Pair) {
	if !p.InvolvesToken(&outputAmount.Token) {
		panic("Invalid token")
	}

	outputReserve := p.ReserveOf(&outputAmount.Token)
	if p.Reserve0().Quotient().BitLen() == 0 ||
		p.Reserve1().Quotient().BitLen() == 0 ||
		outputAmount.Quotient().Cmp(outputReserve.Quotient()) >= 0 {
		logrus.Fatal("insufficient reserves")
		// panic("Insufficient reserves")
	}

	var inputReserve *fractions.TokenAmount

	if outputAmount.Token.Equal(&p.TokenAmount0.Token) {
		inputReserve = p.Reserve1()
	} else {
		inputReserve = p.Reserve0()
	}

	numerator := new(big.Int).Mul(
		new(big.Int).Mul(
			inputReserve.Quotient(),
			outputAmount.Quotient(),
		),
		big.NewInt(constants.N1000),
	)
	denominator := new(big.Int).Mul(
		new(big.Int).Sub(
			outputReserve.Quotient(),
			outputAmount.Quotient(),
		),
		big.NewInt(constants.N997),
	)

	inputAmount := fractions.NewTokenAmount(
		inputReserve.Token,
		new(big.Int).Div(numerator, denominator),
	)

	return inputAmount, *NewPair(inputReserve.Add(inputAmount), outputReserve.Subtract(outputAmount), p.LiquidityToken.Address)
}

// GetLiquidityMinted get the minted LiquidityToken amount depending on the input amounts
func (p *Pair) GetLiquidityMinted(totalSupply, tokenAmountA, tokenAmountB *fractions.TokenAmount) fractions.TokenAmount {
	if !totalSupply.Token.Equal(&p.LiquidityToken) {
		panic("Invalid liquidity total Supply Token")
	}

	var tokenAmounts = make([]fractions.TokenAmount, constants.NTWO)

	if tokenAmountA.Token.SortsBefore(&tokenAmountB.Token) {
		tokenAmounts[0] = *tokenAmountA
		tokenAmounts[1] = *tokenAmountB
	} else {
		tokenAmounts[0] = *tokenAmountB
		tokenAmounts[1] = *tokenAmountA
	}

	if !tokenAmounts[0].Token.Equal(p.Token0()) && tokenAmounts[1].Token.Equal(p.Token1()) {
		panic("Pair tokens mismatch, sortsBefore")
	}

	var liquidity *big.Int

	if totalSupply.Raw().BitLen() == 0 {
		liquidity = liquidity.Sub(liquidity.Sqrt(liquidity.Mul(tokenAmounts[0].Raw(), tokenAmounts[1].Raw())), big.NewInt(constants.N1000))
	} else {
		amount0 := liquidity.Div(liquidity.Mul(tokenAmounts[0].Raw(), totalSupply.Raw()), p.Reserve0().Raw())
		amount1 := liquidity.Div(liquidity.Mul(tokenAmounts[1].Raw(), totalSupply.Raw()), p.Reserve1().Raw())
		if amount0.Cmp(amount1) <= 0 {
			liquidity = amount0
		} else {
			liquidity = amount1
		}
	}

	if liquidity.Cmp(big.NewInt(0)) <= 0 {
		panic("Insufficient input amount")
	}

	return *fractions.NewTokenAmount(p.LiquidityToken, liquidity)
}

// GetLiquidityValue returns the actual value of the LiquidityToken
func (p *Pair) GetLiquidityValue(token *entities.Token, totalSupply, liquidity *fractions.TokenAmount, feeOn bool, last *big.Int) fractions.TokenAmount {
	if !p.InvolvesToken(token) {
		panic("TOKEN")
	}

	if !totalSupply.Token.Equal(&p.LiquidityToken) {
		panic("TOTAL_SUPPLY")
	}

	if !liquidity.Token.Equal(&p.LiquidityToken) {
		panic("LIQUIDITY")
	}

	if liquidity.Raw().Cmp(totalSupply.Raw()) > 0 {
		panic("LIQUIDITY")
	}

	var totalSupplyAdjusted *fractions.TokenAmount

	if !feeOn {
		totalSupplyAdjusted = totalSupply
	} else {
		if last.BitLen() > 0 {
			rootK := big.NewInt(1).Sqrt(big.NewInt(1).Mul(p.Reserve0().Raw(), p.Reserve1().Raw()))
			rootKLast := big.NewInt(1).Sqrt(last)
			if rootK.Cmp(rootKLast) > 0 {
				numerator := big.NewInt(1).Mul(totalSupply.Raw(), big.NewInt(1).Sub(rootK, rootKLast))
				denominator := big.NewInt(1).Add(big.NewInt(1).Mul(rootK, big.NewInt(constants.NFIVE)), rootKLast)
				feeLiquidity := big.NewInt(1).Div(numerator, denominator)
				totalSupplyAdjusted = totalSupply.Add(fractions.NewTokenAmount(p.LiquidityToken, feeLiquidity))
			} else {
				totalSupplyAdjusted = totalSupply
			}
		} else {
			totalSupplyAdjusted = totalSupply
		}
	}

	return *fractions.NewTokenAmount(*token, big.NewInt(1).Div(
		big.NewInt(1).Mul(liquidity.Raw(), p.ReserveOf(token).Raw()),
		totalSupplyAdjusted.Raw(),
	))
}
