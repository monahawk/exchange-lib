package trade

import (
	"math/big"

	"github.com/sirupsen/logrus"
	"gitlab.com/monahawk/exchange-lib/pkg/constants"
	"gitlab.com/monahawk/exchange-lib/pkg/entities"
	"gitlab.com/monahawk/exchange-lib/pkg/entities/fractions"
)

// InputOutput the input and output amount for the trade
type InputOutput struct {
	inputAmount, outputAmount fractions.CurrencyAmount
}

// InputOutputComparator is used to compare the two inuts and outputs
func InputOutputComparator(a, b *InputOutput) int {
	if !a.inputAmount.Currency.Equals(&b.inputAmount.Currency) {
		panic("Invalid input currency")
	}

	if !a.outputAmount.Currency.Equals(&b.outputAmount.Currency) {
		panic("Invalid output currency")
	}

	if a.outputAmount.EqualTo(&b.outputAmount.Fraction) {
		if a.inputAmount.EqualTo(&b.inputAmount.Fraction) {
			return 0
		}

		if a.inputAmount.LessThan(&b.inputAmount.Fraction) {
			return -1
		}

		return 1
	}

	if a.outputAmount.LessThan(&b.outputAmount.Fraction) {
		return 1
	}

	return -1
}

// BestTradeOptions can define how complex the trade calculation should be
type BestTradeOptions struct {
	maxNumResults, maxHops int
}

// FromRoute creates a price from an incoming route
func FromRoute(route *Route) *fractions.Price {
	var prices []fractions.Price

	for i := 0; i < len(route.Pairs); i++ {
		if route.Path[i].Equals(&route.Pairs[i].Token0().Currency) {
			prices = append(prices, *route.Pairs[i].Token0Price())
		} else {
			prices = append(prices, *route.Pairs[i].Token1Price())
		}
	}

	reduced := prices[0]

	remainingPrices := prices[1:]

	for i := range remainingPrices {
		reduced.Multiply(&remainingPrices[i])
	}

	return &reduced
}

func wrappedAmount(currencyAmount *fractions.TokenAmount, chainID constants.ChainID) *fractions.TokenAmount {
	if currencyAmount.Token.Currency.Symbol != entities.ETHER(chainID).Symbol {
		return currencyAmount
	}

	return fractions.NewTokenAmount(entities.WETH[chainID], currencyAmount.Raw())
}

func wrappedCurrency(token entities.Token, chainID constants.ChainID) entities.Token {
	if !token.Currency.Equals(entities.ETHER(chainID)) {
		return token
	}

	return entities.WETH[chainID]
}

// Trade is used for exchange trade deinitions
type Trade struct {
	Route
	TradeType                    constants.TradeType
	InputAmount, OutputAmount    *fractions.CurrencyAmount
	ExecutionPrice, NextMidPrice *fractions.Price
	PriceImpact                  *fractions.Percent
}

// ExactIn calculates a trade for an exact input amount
func ExactIn(route *Route, amountIn *fractions.TokenAmount) *Trade {
	return NewTrade(route, amountIn, constants.ExactInput)
}

// ExactOut calculates a trade for an exact output amount
func ExactOut(route *Route, amountOut *fractions.TokenAmount) *Trade {
	return NewTrade(route, amountOut, constants.ExactOutput)
}

// NewTrade is used to construct a trade from route and tokenamount
func NewTrade(route *Route, amount *fractions.TokenAmount, tradeType constants.TradeType) *Trade {
	var amounts = make([]*fractions.TokenAmount, len(route.Path))

	var nextPairs = make([]Pair, len(route.Path))

	if tradeType == constants.ExactInput {
		if !amount.Token.Currency.Equals(&route.Input.Currency) {
			panic("Wrong input currency")
		}

		amounts[0] = wrappedAmount(amount, route.Input.ChainID)

		for i := 0; i < len(route.Path)-1; i++ {
			pair := route.Pairs[i]
			outputAmount, nextPair := pair.GetOutputAmount(amounts[i])
			amounts[i+1] = outputAmount
			nextPairs[i] = nextPair
		}
	} else {
		if !amount.Token.Currency.Equals(&route.Output.Currency) {
			panic("Wrong Output currency")
		}
		amounts[len(amounts)-1] = wrappedAmount(amount, route.Input.ChainID)
		for i := len(route.Path) - 1; i > 0; i-- {
			pair := route.Pairs[i-1]
			inputAmount, nextPair := pair.GetInputAmount(amounts[i])
			amounts[i-1] = inputAmount
			nextPairs[i-1] = nextPair
		}
	}

	var inputAmount, outputAmount *fractions.TokenAmount

	if tradeType == constants.ExactInput {
		inputAmount = amount
		outputAmount = amounts[len(amounts)-1]
	}

	if tradeType == constants.ExactOutput {
		inputAmount = amounts[0]
		outputAmount = amount
	}

	return &Trade{
		Route:          *route,
		TradeType:      tradeType,
		InputAmount:    inputAmount.AsCurrencyAmount(),
		OutputAmount:   outputAmount.AsCurrencyAmount(),
		ExecutionPrice: fractions.NewPrice(&inputAmount.Token.Currency, &outputAmount.Token.Currency, inputAmount.Raw(), outputAmount.Raw()),
		NextMidPrice:   FromRoute(NewRoute(nextPairs, route.Input, route.Output)),
		PriceImpact:    nil,
	}
}

// MinimumAmountOut defined what is the minimum amount wantd for the trade
func (t *Trade) MinimumAmountOut(slippageTolerance *fractions.Percent) fractions.CurrencyAmount {
	if slippageTolerance.Fraction.Numerator.Cmp(big.NewInt(0)) < 0 {
		panic("Wrong slippage tolerance")
	}

	if t.TradeType == constants.ExactOutput {
		return *t.OutputAmount
	}

	slippageAdjustedAmountOut := fractions.NewFraction(big.NewInt(1), big.NewInt(1)).Add(&slippageTolerance.Fraction).Invert().Multiply(&t.OutputAmount.Fraction).Quotient()

	return *fractions.NewCurrencyAmount(&t.OutputAmount.Currency, slippageAdjustedAmountOut)
}

// MinimumAmountIn defines what is the minimum amount wanted by exact input
func (t *Trade) MinimumAmountIn(slippageTolerance *fractions.Percent) fractions.CurrencyAmount {
	if slippageTolerance.Fraction.Numerator.Cmp(big.NewInt(0)) < 0 {
		panic("Wrong slippage tolerance")
	}

	if t.TradeType == constants.ExactInput {
		return *t.InputAmount
	}

	slippageAdjustedAmountOut := fractions.NewFraction(big.NewInt(1), big.NewInt(1)).Add(&slippageTolerance.Fraction).Invert().Multiply(&t.InputAmount.Fraction).Quotient()

	return *fractions.NewCurrencyAmount(&t.InputAmount.Currency, slippageAdjustedAmountOut)
}

// BestTradeExactIn gets the best trade for the given pairs, when the exact input amount is set
func BestTradeExactIn(
	pairs []Pair,
	currencyAmountIn *fractions.TokenAmount,
	currencyOut entities.Token,
	bestTradeOptions BestTradeOptions,
	currentPairs []Pair,
	originalAmountIn *fractions.TokenAmount,
	bestTrades []Trade,
) []Trade {
	if len(pairs) == 0 {
		panic("PAIRS invalid")
	}

	if bestTradeOptions.maxHops <= 0 {
		panic("Invalid max hops")
	}

	if !originalAmountIn.EqualTo(&currencyAmountIn.Fraction) || len(currentPairs) == 0 {
		panic("INVALID_RECURSION")
	}

	chainID := currencyAmountIn.Token.ChainID

	amountIn := wrappedAmount(currencyAmountIn, chainID)
	tokenOut := wrappedCurrency(currencyOut, chainID)

	for i := 0; i < len(pairs); i++ {
		pair := pairs[i]
		if !pair.Token0().Equal(&amountIn.Token) && !pair.Token1().Equal(&amountIn.Token) {
			continue
		}

		if pair.Reserve0().Numerator.BitLen() == 0 || pair.Reserve1().Numerator.BitLen() == 0 {
			continue
		}

		var amountOut *fractions.TokenAmount

		amountOut, _ = pair.GetOutputAmount(amountIn)

		if err := recover(); err != nil {
			logrus.Errorf("Error while getting output amount %v", err)
			continue
		}

		if amountOut.Token.Equal(&tokenOut) && len(bestTrades) < bestTradeOptions.maxNumResults {
			bestTrades = append(bestTrades, *NewTrade(
				NewRoute(append(currentPairs, pair), originalAmountIn.Token, currencyOut),
				originalAmountIn,
				constants.ExactInput,
			),
			)
		} else if bestTradeOptions.maxHops > 1 && len(pairs) > 1 {
			pairsExcludingThisPair := pairs[1:]
			BestTradeExactIn(
				pairsExcludingThisPair,
				amountOut,
				currencyOut,
				bestTradeOptions,
				append(currentPairs, pair),
				originalAmountIn,
				bestTrades,
			)
		}
	}

	return bestTrades
}
