package entities

import (
	"testing"

	"github.com/stretchr/testify/require"
	"gitlab.com/monahawk/exchange-lib/pkg/constants"
)

var (
	token1 = NewToken(constants.BSCMainnet,
		"0x123123123123",
		6,
		"Test1",
		"TO",
	)
	token2 = NewToken(constants.BSCMainnet,
		"0x12312312312323",
		6,
		"Test2",
		"TT",
	)
)

func TestEqual(t *testing.T) {
	require := require.New(t)

	isEqual := token1.Equal(&token2)
	require.False(isEqual)

	isEqual = token1.Equal(&token1)
	require.True(isEqual)
}
