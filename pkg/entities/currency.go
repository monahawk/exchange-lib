package entities

import "gitlab.com/monahawk/exchange-lib/pkg/constants"

type Currency struct {
	Decimals int    `json:"decimals,omitempty" bson:"decimals"`
	Symbol   string `json:"symbol,omitempty" bson:"symbol"`
	Name     string `json:"name,omitempty" bson:"name"`
}

func newCurrency(decimals int, symbol, name string) *Currency {
	if decimals <= 0 && decimals > 255 {
		panic("invalid decimal for currency")
	}

	return &Currency{
		Decimals: decimals,
		Symbol:   symbol,
		Name:     name,
	}
}

func (c *Currency) Equals(other *Currency) bool {
	if c == other {
		return true
	}

	return c.Name == other.Name && c.Symbol == other.Symbol
}

func ETHER(chainID constants.ChainID) *Currency {
	switch chainID {
	case constants.MAINNET, constants.SHASTA, constants.NILE:
		return &TRON
	case constants.BSCMainnet, constants.BSCTestnet:
		return &BNB
	case constants.BTTMainnet, constants.BTTTestnet:
		return &BTT
	}

	return nil
}

var (
	TRONDecimals  = 6
	ISWAPDecimals = 8
	BTTDecimals   = 18
	BNBDecimals   = 18
	TRON          = *newCurrency(TRONDecimals, "TRX", "Tron")
	BNB           = *newCurrency(BNBDecimals, "BNB", "Tron")
	BTT           = *newCurrency(BTTDecimals, "BTT", "Tron")
)
