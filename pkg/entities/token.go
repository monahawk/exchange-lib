package entities

import (
	"strings"

	"gitlab.com/monahawk/exchange-lib/pkg/constants"
)

// Token the ERC20 based crypto token
type Token struct {
	Currency `bson:"inline"`
	ChainID  constants.ChainID `json:"chain_id,omitempty" bson:"chainId"`
	Address  string            `json:"address,omitempty" bson:"address"`
}

// NewToken used as the main constructior for tokens
func NewToken(chainID constants.ChainID, address string, decimals int, symbol, name string) Token {
	return Token{
		Currency: *newCurrency(decimals, symbol, name),
		ChainID:  chainID,
		Address:  address,
	}
}

// SortsBefore sorts the tokens based on addresses
func (t *Token) SortsBefore(other *Token) bool {
	if t.ChainID != other.ChainID {
		panic("wrong chain IDs")
	}

	if t.Address == other.Address {
		panic("same Address")
	}

	return strings.ToLower(t.Address) < strings.ToLower(other.Address)
}

// Equal checking the token equalitiy
func (t *Token) Equal(other *Token) bool {
	if t == other {
		return true
	}

	return t.ChainID == other.ChainID && t.Address == other.Address
}

// Wrapped returns the wrapped token
func (t *Token) Wrapped() *Token {
	return t
}

// CurrencyEquals checks the currency equality
func CurrencyEquals(currencyA, currencyB Currency) bool {
	return currencyA.Equals(&currencyB)
}
