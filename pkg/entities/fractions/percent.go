package fractions

import (
	"math/big"

	"gitlab.com/monahawk/exchange-lib/pkg/constants"
)

type Percent struct {
	Fraction
	isPercent bool
}

var OneHundredPercent = Percent{
	Fraction:  *NewFraction(big.NewInt(constants.NHUNDRED), big.NewInt(1)),
	isPercent: true,
}

func ToPercent(fraction *Fraction) *Percent {
	return &Percent{
		Fraction:  *fraction,
		isPercent: true,
	}
}

func (p *Percent) Add(other *Percent) *Percent {
	add := p.Fraction.Add(&other.Fraction)
	return ToPercent(add)
}

func (p *Percent) Subtract(other *Percent) *Percent {
	sub := p.Fraction.Subtract(&other.Fraction)
	return ToPercent(sub)
}

func (p *Percent) Multiply(other *Percent) *Percent {
	mul := p.Fraction.Multiply(&other.Fraction)
	return ToPercent(mul)
}

func (p *Percent) Divide(other *Percent) *Percent {
	div := p.Fraction.Divide(&other.Fraction)
	return ToPercent(div)
}

func (p *Percent) ToSignificant(significantDigits int, format interface{}, rounding Rounding) string {
	return p.Multiply(&OneHundredPercent).Fraction.ToSignificant(significantDigits, format, rounding)
}

func (p *Percent) ToFixed(decimalPlaces int, format interface{}, rounding big.RoundingMode) string {
	return p.Multiply(&OneHundredPercent).Fraction.ToFixed(decimalPlaces, format, rounding)
}
