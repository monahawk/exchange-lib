package fractions

import (
	"math/big"
	"reflect"
	"testing"
)

type TestInput struct {
	fraction *Fraction
	expected *big.Int
}

func TestQuotient(t *testing.T) {
	t.Parallel()

	var quotientTests = []TestInput{
		{NewFraction(big.NewInt(8), big.NewInt(3)), big.NewInt(2)},
		{NewFraction(big.NewInt(8), big.NewInt(3)), big.NewInt(2)},
		{NewFraction(big.NewInt(8), big.NewInt(3)), big.NewInt(2)},
	}

	for _, v := range quotientTests {
		if v.fraction.Quotient().Cmp(v.expected) != 0 {
			t.Errorf("Error while getting quotient. \nGot: %v \nExpected: %v\n", *v.fraction.Numerator, *v.expected)
		}
	}
}

func TestToSignificant(t *testing.T) {
	t.Parallel()

	var toSignificantTests = []TestInput{
		{NewFraction(big.NewInt(8), big.NewInt(3)), big.NewInt(2)},
		{NewFraction(big.NewInt(8), big.NewInt(3)), big.NewInt(2)},
		{NewFraction(big.NewInt(8), big.NewInt(3)), big.NewInt(2)},
	}

	for _, v := range toSignificantTests {
		if v.fraction.Quotient().Cmp(v.expected) != 0 {
			t.Errorf("Error while getting quotient. \nGot: %v \nExpected: %v\n", *v.fraction.Numerator, *v.expected)
		}
	}
}

func TestFraction_Invert(t *testing.T) {
	type fields struct {
		Numerator   *big.Int
		Denominator *big.Int
	}

	tests := []struct {
		name   string
		fields fields
		want   *Fraction
	}{
		{
			name: "Half to two",
			fields: fields{
				Numerator:   big.NewInt(1),
				Denominator: big.NewInt(2),
			},
			want: &Fraction{
				Numerator:   big.NewInt(2),
				Denominator: big.NewInt(1),
			},
		},
		{
			name: "0.01 to 100",
			fields: fields{
				Numerator:   big.NewInt(1),
				Denominator: big.NewInt(100),
			},
			want: &Fraction{
				Numerator:   big.NewInt(100),
				Denominator: big.NewInt(1),
			},
		},
		{
			name: "Third to 3",
			fields: fields{
				Numerator:   big.NewInt(1),
				Denominator: big.NewInt(3),
			},
			want: &Fraction{
				Numerator:   big.NewInt(3),
				Denominator: big.NewInt(1),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &Fraction{
				Numerator:   tt.fields.Numerator,
				Denominator: tt.fields.Denominator,
			}
			if got := f.Invert(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Fraction.Invert() = %v, want %v", got, tt.want)
			}
		})
	}
}

type fields struct {
	Numerator   *big.Int
	Denominator *big.Int
}

type args struct {
	other *Fraction
}

type tests struct {
	name   string
	fields fields
	args   args
	want   *Fraction
}

//nolint:dupl // only a test file
func TestFraction_Add(t *testing.T) {
	tests := []tests{
		{
			name: "Add with same denominator",
			fields: fields{
				Numerator:   big.NewInt(2),
				Denominator: big.NewInt(1),
			},
			args: args{
				other: NewFraction(big.NewInt(2), big.NewInt(1)),
			},
			want: NewFraction(big.NewInt(4), big.NewInt(1)),
		},
		{
			name: "Add with different denominator",
			fields: fields{
				Numerator:   big.NewInt(2),
				Denominator: big.NewInt(3),
			},
			args: args{
				other: NewFraction(big.NewInt(2), big.NewInt(2)),
			},
			want: NewFraction(big.NewInt(10), big.NewInt(6)),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &Fraction{
				Numerator:   tt.fields.Numerator,
				Denominator: tt.fields.Denominator,
			}
			if got := f.Add(tt.args.other); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Fraction.Add() = %v, want %v", got, tt.want)
			}
		})
	}
}

//nolint:dupl // only a test file
func TestFraction_Subtract(t *testing.T) {
	tests := []tests{
		{
			name: "Subtract with same denominator",
			fields: fields{
				Numerator:   big.NewInt(2),
				Denominator: big.NewInt(1),
			},
			args: args{
				other: NewFraction(big.NewInt(1), big.NewInt(1)),
			},
			want: NewFraction(big.NewInt(1), big.NewInt(1)),
		},
		{
			name: "Subtract with different denominator",
			fields: fields{
				Numerator:   big.NewInt(2),
				Denominator: big.NewInt(3),
			},
			args: args{
				other: NewFraction(big.NewInt(1), big.NewInt(2)),
			},
			want: NewFraction(big.NewInt(1), big.NewInt(6)),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := &Fraction{
				Numerator:   tt.fields.Numerator,
				Denominator: tt.fields.Denominator,
			}
			if got := f.Subtract(tt.args.other); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Fraction.Subtract() = %v, want %v", got, tt.want)
			}
		})
	}
}
