package fractions

import (
	"math/big"

	"github.com/leekchan/accounting"
	"github.com/shopspring/decimal"
)

type Rounding int

const (
	RoundDown Rounding = iota
	RoundHalfUp
	RoundUp
)

type Fraction struct {
	Numerator   *big.Int `json:"numerator,omitempty"`
	Denominator *big.Int `json:"denominator,omitempty"`
}

func NewFraction(numerator, denominator *big.Int) *Fraction {
	return &Fraction{
		Numerator:   numerator,
		Denominator: denominator,
	}
}

func (f *Fraction) Quotient() *big.Int {
	return new(big.Int).Div(f.Numerator, f.Denominator)
}

func (f *Fraction) Remainder() *Fraction {
	return NewFraction(big.NewInt(1).Rem(f.Numerator, f.Denominator), f.Denominator)
}

func (f *Fraction) Invert() *Fraction {
	return NewFraction(f.Denominator, f.Numerator)
}

func (f *Fraction) Add(other *Fraction) *Fraction {
	var res big.Int
	if f.Denominator.Cmp(other.Denominator) == 0 {
		return NewFraction(res.Add(f.Numerator, other.Numerator), f.Denominator)
	}

	return NewFraction(res.Add(
		big.NewInt(1).Mul(f.Numerator, other.Denominator),
		big.NewInt(1).Mul(other.Numerator, f.Denominator),
	), big.NewInt(1).Mul(f.Denominator, other.Denominator))
}

func (f *Fraction) Subtract(other *Fraction) *Fraction {
	var res big.Int
	if f.Denominator.Cmp(other.Denominator) == 0 {
		return NewFraction(res.Sub(f.Numerator, other.Numerator), f.Denominator)
	}

	return NewFraction(res.Sub(
		big.NewInt(1).Mul(f.Numerator, other.Denominator),
		big.NewInt(1).Mul(other.Numerator, f.Denominator),
	), big.NewInt(1).Mul(f.Denominator, other.Denominator))
}

func (f *Fraction) LessThan(other *Fraction) bool {
	var a, b big.Int

	a.Mul(f.Numerator, other.Denominator)
	b.Mul(other.Numerator, f.Denominator)

	return a.Cmp(&b) < 0
}

func (f *Fraction) EqualTo(other *Fraction) bool {
	var a, b big.Int

	a.Mul(f.Numerator, other.Denominator)
	b.Mul(other.Numerator, f.Denominator)

	return a.Cmp(&b) == 0
}

func (f *Fraction) GreaterThan(other *Fraction) bool {
	var a, b big.Int

	a.Mul(f.Numerator, other.Denominator)
	b.Mul(other.Numerator, f.Denominator)

	return a.Cmp(&b) > 0
}

func (f *Fraction) Multiply(other *Fraction) *Fraction {
	return NewFraction(
		big.NewInt(1).Mul(f.Numerator, other.Numerator),
		big.NewInt(1).Mul(f.Denominator, other.Denominator),
	)
}

func (f *Fraction) Divide(other *Fraction) *Fraction {
	return NewFraction(
		big.NewInt(1).Mul(f.Numerator, other.Denominator),
		big.NewInt(1).Mul(f.Denominator, other.Numerator),
	)
}

func (f *Fraction) ToSignificant(significantDigits int, format interface{}, rounding Rounding) string {
	if significantDigits < 0 {
		panic("the significant digits must be positive")
	}

	ac := accounting.Accounting{
		Symbol:    "",
		Precision: significantDigits,
	}

	return ac.FormatMoneyDecimal(f.AsDecimal())
}

func (f *Fraction) ToFixed(decimalPlaces int, format interface{}, rounding big.RoundingMode) string {
	if decimalPlaces < 0 {
		panic("the significant digits must be positive")
	}

	ac := accounting.Accounting{
		Symbol:    "",
		Precision: decimalPlaces,
	}

	return ac.FormatMoneyDecimal(decimal.NewFromBigInt(f.Numerator, 0).Div(decimal.NewFromBigInt(f.Denominator, 0)))
}

func (f *Fraction) AsFraction() *Fraction {
	return NewFraction(f.Numerator, f.Denominator)
}

func (f *Fraction) AsDecimal() decimal.Decimal {
	if f.Denominator.Cmp(big.NewInt(0)) == 0 {
		return decimal.NewFromInt32(0)
	}

	return decimal.NewFromBigInt(f.Numerator, 0).Div(decimal.NewFromBigInt(f.Denominator, 0))
}
