package fractions

import (
	"math/big"

	"gitlab.com/monahawk/exchange-lib/pkg/constants"
	"gitlab.com/monahawk/exchange-lib/pkg/entities"
)

type Price struct {
	Fraction
	BaseCurrency  entities.Currency `json:"base_currency,omitempty"`
	QuoteCurrency entities.Currency `json:"quote_currency,omitempty"`
	Scalar        Fraction          `json:"scalar,omitempty"`
}

func NewPrice(baseCurrency, quoteCurrency *entities.Currency, denominator, numerator *big.Int) *Price {
	scalar := NewFraction(
		big.NewInt(1).Exp(big.NewInt(constants.NTEN), big.NewInt(int64(baseCurrency.Decimals)), nil),
		big.NewInt(1).Exp(big.NewInt(constants.NTEN), big.NewInt(int64(quoteCurrency.Decimals)), nil),
	)

	return &Price{
		Fraction:      *NewFraction(numerator, denominator),
		BaseCurrency:  *baseCurrency,
		QuoteCurrency: *quoteCurrency,
		Scalar:        *scalar,
	}
}

func (p *Price) Raw() *Fraction {
	return NewFraction(p.Numerator, p.Denominator)
}

func (p *Price) Adjusted() *Fraction {
	return NewFraction(p.Numerator, p.Denominator).Multiply(&p.Scalar)
}

func (p *Price) Invert() *Price {
	return NewPrice(&p.QuoteCurrency, &p.BaseCurrency, p.Numerator, p.Denominator)
}

func (p *Price) Multiply(other *Price) *Price {
	if p.QuoteCurrency.Equals(&other.BaseCurrency) {
		panic("Tokens are not matching")
	}

	fraction := p.Fraction.Multiply(&other.Fraction)

	return NewPrice(&p.BaseCurrency, &other.QuoteCurrency, fraction.Denominator, fraction.Numerator)
}

func (p *Price) Quote(currencyAmount CurrencyAmount) *CurrencyAmount {
	return NewCurrencyAmount(&p.QuoteCurrency, p.Fraction.Multiply(&currencyAmount.Fraction).Quotient())
}

func (p *Price) AdjustedForDecimals() *Fraction {
	return p.Fraction.Multiply(&p.Scalar)
}

func (p *Price) ToSignificant(significantDigits int, format interface{}, rounding Rounding) string {
	return p.AdjustedForDecimals().ToSignificant(significantDigits, format, rounding)
}

func (p *Price) ToFixed(decimalPlaces int, format interface{}, rounding big.RoundingMode) string {
	return p.AdjustedForDecimals().ToFixed(decimalPlaces, format, rounding)
}
