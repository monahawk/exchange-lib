package fractions

import (
	"math/big"

	"gitlab.com/monahawk/exchange-lib/pkg/constants"
	"gitlab.com/monahawk/exchange-lib/pkg/entities"
)

type CurrencyAmount struct {
	Fraction
	entities.Currency
}

func (ca *CurrencyAmount) Raw() *big.Int {
	return ca.Numerator
}

func NewCurrencyAmount(currency *entities.Currency, amount *big.Int) *CurrencyAmount {
	return &CurrencyAmount{
		Fraction: *NewFraction(amount, big.NewInt(1).Exp(big.NewInt(constants.NTEN), big.NewInt(int64(currency.Decimals)), big.NewInt(0))),
		Currency: *currency,
	}
}

func (ca *CurrencyAmount) Add(other *CurrencyAmount) *CurrencyAmount {
	if ca.Currency.Equals(&other.Currency) {
		panic("Differenc currencies cannot be added")
	}

	var add big.Int

	return NewCurrencyAmount(
		&ca.Currency,
		add.Add(ca.Numerator, other.Numerator),
	)
}

func (ca *CurrencyAmount) Subtract(other *CurrencyAmount) *CurrencyAmount {
	if ca.Currency.Equals(&other.Currency) {
		panic("Different currencies cannot be subtracted")
	}

	var add big.Int

	return NewCurrencyAmount(
		&ca.Currency,
		add.Sub(ca.Numerator, other.Numerator),
	)
}

func (ca *CurrencyAmount) ToSignificant(significantDigits int, format interface{}, rounding Rounding) string {
	return ca.Fraction.ToSignificant(significantDigits, format, rounding)
}

func (ca *CurrencyAmount) ToFixed(decimalPlaces int, format interface{}, rounding big.RoundingMode) string {
	return ca.Fraction.ToFixed(decimalPlaces, format, rounding)
}

func (ca *CurrencyAmount) ToExact(format interface{}) string {
	return big.NewRat(ca.Numerator.Int64(), ca.Denominator.Int64()).FloatString(ca.Decimals)
}
