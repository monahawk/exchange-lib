package fractions

import (
	"math/big"

	"gitlab.com/monahawk/exchange-lib/pkg/constants"
	"gitlab.com/monahawk/exchange-lib/pkg/entities"
)

type TokenAmount struct {
	Fraction
	entities.Token
}

func NewTokenAmount(token entities.Token, amount *big.Int) *TokenAmount {
	return &TokenAmount{
		Fraction: *NewFraction(amount, new(big.Int).Exp(big.NewInt(constants.NTEN), big.NewInt(int64(token.Decimals)), big.NewInt(0))),
		Token:    token,
	}
}

func (ta *TokenAmount) Raw() *big.Int {
	return ta.Numerator
}

func (ta *TokenAmount) AsCurrencyAmount() *CurrencyAmount {
	return NewCurrencyAmount(&entities.Currency{
		Decimals: ta.Decimals,
		Symbol:   ta.Symbol,
		Name:     ta.Name,
	}, ta.Numerator)
}

func (ta *TokenAmount) Add(other *TokenAmount) *TokenAmount {
	if !ta.Token.Equal(&other.Token) {
		panic("Tokens must be equal")
	}

	var add big.Int

	return NewTokenAmount(
		ta.Token,
		add.Add(ta.Numerator, other.Numerator),
	)
}

func (ta *TokenAmount) Subtract(other *TokenAmount) *TokenAmount {
	if !ta.Token.Equal(&other.Token) {
		panic("Tokens must be equal")
	}

	var add big.Int

	return NewTokenAmount(
		ta.Token,
		add.Sub(ta.Numerator, other.Numerator),
	)
}
