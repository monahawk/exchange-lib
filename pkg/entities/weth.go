package entities

import "gitlab.com/monahawk/exchange-lib/pkg/constants"

var WETH = map[constants.ChainID]Token{
	constants.MAINNET:    NewToken(constants.MAINNET, "0x891CDB91D149F23B1A45D9C5CA78A88D0CB44C18", TRONDecimals, "WTRX", "Wrapped TRX"),
	constants.SHASTA:     NewToken(constants.SHASTA, "0xB970B980C520EC3F49921C2727BFA6DE79E7226A", TRONDecimals, "WTRX", "Wrapped TRX"),
	constants.NILE:       NewToken(constants.NILE, "0x8f44113A985076431b77f6078f0929f949cB8836", TRONDecimals, "WTRX", "Wrapped TRX"),
	constants.BTTMainnet: NewToken(constants.BTTMainnet, "0x23181F21DEa5936e24163FFABa4Ea3B316B57f3C", BTTDecimals, "WBTT", "Wrapped Bittorrent"),
	constants.BSCMainnet: NewToken(constants.BSCMainnet, "0xbb4CdB9CBd36B01bD1cBaEBF2De08d9173bc095c", BNBDecimals, "WBNB", "Wrapped BNB"),
}
